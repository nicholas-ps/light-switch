from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name="index"),
    path('light-on/', lightOn, name="light-on"),
    path('light-off/', lightOff, name="light-off"),
    path('sensor-on/', sensorOn, name="sensor-on"),
    path('sensor-off/', sensorOff, name="sensor-off")
]