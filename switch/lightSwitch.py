from RPi import GPIO

class LightSwitch():
    def __init__(self, pin):
        self.PIN = pin
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.PIN, GPIO.OUT)

    def on(self):
        GPIO.output(self.PIN, GPIO.LOW)

    def off(self):
        GPIO.output(self.PIN, GPIO.HIGH)

    def cleanup(self):
        GPIO.output(self.PIN, GPIO.HIGH)
        
