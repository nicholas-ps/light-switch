from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from .lightSwitch import LightSwitch
import subprocess

# Create your views here.
response = {}
lightSwitch = LightSwitch(8)

def index(request):
    html = "switch.html"
    lightSwitch.cleanup()
    return render(request, html, response)

def lightOn(request):
    lightSwitch.on()
    return JsonResponse({"message" : "SUCCESS"})

def lightOff(request):
    lightSwitch.off()
    return JsonResponse({"message" : "SUCCESS"})

def sensorOn(request):
    turn_on_service_ldr()
    return JsonResponse({"message" : "SUCCESS"})

def sensorOff(request):
    turn_off_service_ldr()
    return JsonResponse({"message" : "SUCCESS"})


def turn_on_service_ldr():
    subprocess.call(['sudo', 'systemctl', 'start', 'sysprog_ldr_lampu'])
def turn_off_service_ldr():
    subprocess.call(['sudo', 'systemctl', 'stop', 'sysprog_ldr_lampu'])
